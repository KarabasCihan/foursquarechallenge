//
//  PlaceDetailsVC.h
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
#import "FoursquareBackendService.h"

@interface PlaceDetailsVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) FoursquareBackendService *backendService;
@property (nonatomic, strong) Place *place;
@property (nonatomic, strong) NSArray<Tip*> *tips;

@end
