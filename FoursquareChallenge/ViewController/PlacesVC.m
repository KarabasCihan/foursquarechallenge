//
//  PlacesVC.m
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import "PlacesVC.h"
#import "PlaceDetailsVC.h"

@interface PlacesVC ()<UIPopoverPresentationControllerDelegate>

@end

@implementation PlacesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.backendService = [FoursquareBackendService sharedInstance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.places.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceCell" forIndexPath:indexPath];
    UILabel *titleLabel = [cell viewWithTag:1];
    UILabel *addressLabel = [cell viewWithTag:2];
    UILabel *cityLabel = [cell viewWithTag:3];
    UILabel *countryLabel = [cell viewWithTag:4];
    
    Place *place = self.places[indexPath.row];
    titleLabel.text = place.title;
    addressLabel.text = place.address;
    cityLabel.text = place.city;
    countryLabel.text = place.country;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedPlace = self.places[indexPath.row];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.selectedPlace) {
            [self performSegueWithIdentifier:@"showPlaceDetailsVC" sender:self];
        }
    });
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    self.view.alpha = 1.0;
}

- (void)presentationController:(UIPresentationController *)presentationController willPresentWithAdaptiveStyle:(UIModalPresentationStyle)style transitionCoordinator:(id<UIViewControllerTransitionCoordinator>)transitionCoordinator {
    self.view.alpha = 0.2;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showPlaceDetailsVC"]) {
        PlaceDetailsVC *placeDetailsVC = [segue destinationViewController];
        placeDetailsVC.place = self.selectedPlace;
        UIViewController* controller = placeDetailsVC;
        controller.modalPresentationStyle = UIModalPresentationPopover;
        controller.preferredContentSize = CGSizeMake(self.view.frame.size.width * 0.95, self.view.frame.size.height * 0.90);
        
        // configure popover style & delegate
        UIPopoverPresentationController *popover =  controller.popoverPresentationController;
        popover.delegate = self;
        popover.sourceView = self.view;
        popover.sourceRect = CGRectMake(0,0,1,1);
    }
}


@end
