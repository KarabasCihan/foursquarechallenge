//
//  PlaceDetailsVC.m
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import "PlaceDetailsVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PlaceDetailsVC ()

@property (weak, nonatomic) IBOutlet UIImageView *placeImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end

@implementation PlaceDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.backendService = [FoursquareBackendService sharedInstance];
    [self fetchPlaceImage];
    [self fetchPlaceTips];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchPlaceImage {
    [self.backendService fetchImageURL:self.place withCompletion:^(NSString *imageURLString, NSError *error) {
        if (error) {
            NSLog(error.localizedDescription);
        } else {
            if (imageURLString) {
                NSURL *imageURL = [NSURL URLWithString:imageURLString];
                if (imageURL) {
                    [self.placeImageView sd_setImageWithURL:[NSURL URLWithString:imageURLString]];
                }
            } else {
                NSLog(@"Error");
            }
        }
    }];
}

- (void)fetchPlaceTips {
    [self showLoadingView];
    [self.backendService fetchTips:self.place withCompletion:^(NSArray<Tip *> *tips, NSError *error) {
        if (error) {
            NSLog(error.localizedDescription);
        } else {
            if (tips.count > 0) {
                self.tips = tips;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
            } else {
                NSLog(@"Error");
            }
        }
        [self hideLoadingView];
    }];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tips.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TipCell" forIndexPath:indexPath];
    UILabel *textLabel = [cell viewWithTag:1];
    Tip *tip = self.tips[indexPath.row];
    textLabel.text = tip.text;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)showLoadingView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView setHidden:YES];
        [self.activityIndicatorView setHidden:NO];
        [self.activityIndicatorView startAnimating];
    });
}

- (void)hideLoadingView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicatorView setHidden:YES];
        [self.activityIndicatorView stopAnimating];
        
        if (self.tips.count > 0) {
            [self.tableView setHidden:NO];
        } else {
            [self.tableView setHidden:YES];
        }
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
