//
//  MainVC.h
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoursquareBackendService.h"
#import "PlacesVC.h"

@interface MainVC : UIViewController

@property (nonatomic, assign) FoursquareBackendService *backendService;
@property (nonatomic, strong) NSArray<Place*> *places;

@end
