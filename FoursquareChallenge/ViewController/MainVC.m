//
//  MainVC.m
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import "MainVC.h"

@interface MainVC ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UITextField *regionTextField;
@property (weak, nonatomic) IBOutlet UITextField *placeTypeTextField;

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.backendService = [FoursquareBackendService sharedInstance];
    [self hideLoadingView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchPressed:(id)sender {
    [self showLoadingView];
    
    NSString *place = @"Istanbul";
    if (self.regionTextField.text.length > 0) {
        place = self.regionTextField.text;
    }
    
    [self.backendService fetchPlaces:place withCompletion:^(NSArray<Place *> *places, NSError *error) {
        [self hideLoadingView];
        
        if (error) {
            [self showErrorMessage:error.localizedDescription];
        } else {
            if (places.count > 0) {
                self.places = places;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSegueWithIdentifier:@"showPlacesVC" sender:self];
                });
            } else {
                [self showErrorMessage:@"Bilinmeyen bir hata oluştu."];
            }
        }
    }];
}

- (void)showLoadingView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicatorView setHidden:NO];
        [self.activityIndicatorView startAnimating];
    });
}

- (void)hideLoadingView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicatorView setHidden:YES];
        [self.activityIndicatorView stopAnimating];
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showPlacesVC"]) {
        PlacesVC *placesVC = [segue destinationViewController];
        placesVC.places = self.places;
    }
}

- (void)showErrorMessage:(NSString*)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"Hata!"
                                                                                 message:message
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:defaultAction];
        [self presentViewController:alertController animated:YES completion:nil];
    });
}


@end
