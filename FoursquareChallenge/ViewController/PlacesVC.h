//
//  PlacesVC.h
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoursquareBackendService.h"
#import "Place.h"
#import "Tip.h"

@interface PlacesVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) FoursquareBackendService *backendService;
@property (nonatomic, strong) NSArray<Place*> *places;
@property (nonatomic, strong) Place *selectedPlace;

@end
