//
//  HardcodedBackendService.m
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import "HardcodedBackendService.h"

@implementation HardcodedBackendService

//+ (HardcodedBackendService *)sharedInstance {
//    static HardcodedBackendService *sharedInstance = nil;
//    static dispatch_once_t oncePredicate;
//    dispatch_once(&oncePredicate, ^{
//        sharedInstance = [[self alloc] init];
//    });
//    
//    return sharedInstance;
//}
//
//
//- (void)fetchPlaces:(Completion)withCompletion {
//    Place *place = [[Place alloc] init];
//    place.venueId = @"12319236";
//    place.title = @"Vefakar Cafe";
//    place.address = @"Süleymaniye Istanbul";
//    place.city = @"Istanbul";
//    place.country = @"Turkey";
//    
//    NSMutableArray *places = [[NSMutableArray alloc] init];
//    
//    for (int i=0; i<10; i++) {
//        [places addObject:place];
//    }
//    withCompletion(places, nil);
//}
//
//- (void)fetchTips:(Place*)place withCompletion:(TipsCompletionBlock)withCompletion {
//    withCompletion(nil, nil);
//}
//
//- (void)fetchImageURL:(Place*)place withCompletion:(ImageURLStringCompletionBlock)withCompletion {
//    withCompletion(nil, nil);
//}

@end
