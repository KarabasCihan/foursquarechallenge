//
//  FoursquareBackendService.h
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tip.h"
#import "Place.h"

#define CLIENT_ID @"BHUT1BK5K4WVYLVYXRGCE2IZN0NTFQLEJZZI05ZRCRENTXDK"
#define CLIENT_SECRET @"KCK2AJVSCG14YN4Q40N4IRS2OVCFST231EASPLX2GIGXZ4JR"

typedef void(^PlaceCompletionBlock)(NSArray<Place*> *places, NSError *error);
typedef void(^TipsCompletionBlock)(NSArray<Tip*> *tips, NSError *error);
typedef void(^ImageURLStringCompletionBlock)(NSString *imageURLString, NSError *error);

@interface FoursquareBackendService : NSObject

+(FoursquareBackendService *)sharedInstance;

- (void)fetchPlaces:(NSString*)near withCompletion:(PlaceCompletionBlock)withCompletion;
- (void)fetchTips:(Place*)place withCompletion:(TipsCompletionBlock)withCompletion;
- (void)fetchImageURL:(Place*)place withCompletion:(ImageURLStringCompletionBlock)withCompletion;

@end
