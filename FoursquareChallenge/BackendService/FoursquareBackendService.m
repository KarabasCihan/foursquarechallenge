//
//  FoursquareBackendService.m
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import "FoursquareBackendService.h"

@implementation FoursquareBackendService

+ (FoursquareBackendService *)sharedInstance {
    static FoursquareBackendService *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (void)fetchPlaces:(NSString*)near withCompletion:(PlaceCompletionBlock)withCompletion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/search?near=%@&client_id=%@&client_secret=%@&v=20180202", near, CLIENT_ID, CLIENT_SECRET];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (data) {
                                          NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                          
                                          NSArray *venues = resultDictionary[@"response"][@"venues"];
                                          
                                          if (venues.count > 0) {
                                              NSMutableArray<Place*> *places = [[NSMutableArray alloc] init];
                                              
                                              for (int i=0; i<venues.count; i++) {
                                                  NSString *venueId = venues[i][@"id"];
                                                  NSString *name = venues[i][@"name"];
                                                  NSString *address = venues[i][@"location"][@"address"];
                                                  NSString *city = venues[i][@"location"][@"city"];
                                                  NSString *country = venues[i][@"location"][@"country"];
                                                  
                                                  if (venueId && name && address && city && country) {
                                                      Place *place = [[Place alloc] init];
                                                      place.venueId = venueId;
                                                      place.title = name;
                                                      place.address = address;
                                                      place.city = city;
                                                      place.country = country;
                                                      
                                                      [places addObject:place];
                                                  }
                                              }
                                              
                                              withCompletion(places, nil);
                                          } else {
                                              NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"There is no venue"}];
                                              withCompletion(nil, error);
                                          }
                                      } else {
                                          NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"There is no venue"}];
                                          withCompletion(nil, error);
                                      }
                                  }];
    [task resume];
}

- (void)fetchTips:(Place*)place withCompletion:(TipsCompletionBlock)withCompletion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/%@/tips?&client_id=%@&client_secret=%@&v=20180202", place.venueId, CLIENT_ID, CLIENT_SECRET];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          withCompletion(nil, error);
                                      } else {
                                          if (data) {
                                              NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                              NSArray *tips = resultDictionary[@"response"][@"tips"][@"items"];
                                              if (tips.count > 0) {
                                                  NSMutableArray<Tip*> *tempTips = [[NSMutableArray alloc] init];
                                                  for (int i=0; i<tips.count; i++) {
                                                      Tip *tip = [[Tip alloc] init];
                                                      tip.text = tips[i][@"text"];
                                                      
                                                      [tempTips addObject:tip];
                                                  }
                                                  
                                                  if (tempTips) {
                                                      withCompletion(tempTips, nil);
                                                  } else {
                                                      NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"There is no tip"}];
                                                      withCompletion(nil, error);
                                                  }
                                              } else {
                                                  NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"There is no tip"}];
                                                  withCompletion(nil, error);
                                              }
                                          } else {
                                              NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"There is no tip"}];
                                              withCompletion(nil, error);
                                          }
                                      }
                                  }];
    [task resume];
}

- (void)fetchImageURL:(Place*)place withCompletion:(ImageURLStringCompletionBlock)withCompletion {

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/%@/photos?&client_id=%@&client_secret=%@&v=20180302",place.venueId, CLIENT_ID, CLIENT_SECRET];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
     
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (data) {
                                          NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                          NSArray *photos = resultDictionary[@"response"][@"photos"][@"items"];
                                          if (photos.count > 0) {
                                              
                                              NSString *prefix = photos.firstObject[@"prefix"];
                                              NSString *suffix = photos.firstObject[@"suffix"];
                                              
                                              if (prefix && suffix) {
                                                  NSString *imageURLString = [NSString stringWithFormat:@"%@%dx%d%@", prefix, 300, 200, suffix];
                                                  withCompletion(imageURLString, nil);
                                              } else {
                                                  NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"There is no photo"}];
                                                  withCompletion(nil, error);
                                              }
                                          } else {
                                              NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"There is no photo"}];
                                              withCompletion(nil, error);
                                          }
                                      } else {
                                          NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"There is no photo"}];
                                          withCompletion(nil, error);
                                      }
                                  }];
    [task resume];
}

@end
