//
//  HardcodedBackendService.h
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HardcodedBackendService : NSObject

//+(HardcodedBackendService *)sharedInstance;

//- (void)fetchPlaces:(Completion)withCompletion;
//- (void)fetchTips:(Place*)place withCompletion:(TipsCompletionBlock)withCompletion;
//- (void)fetchImageURL:(Place*)place withCompletion:(ImageURLStringCompletionBlock)withCompletion;

@end
