//
//  BackendService.h
//  FoursquareChallenge
//
//  Created by Cihan Karabas on 03/02/2018.
//  Copyright © 2018 cihan. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "Place.h"
//#import "Tip.h"

//typedef void(^Completion)(NSArray<Place*> *places, NSError *error);
//typedef void(^TipsCompletionBlock)(NSArray<Tip*> *tips, NSError *error);
//typedef void(^ImageURLStringCompletionBlock)(NSString *imageURLString, NSError *error);

@protocol BackendService <NSObject>

//@required
//- (void)fetchPlaces:(Completion)withCompletion;
//
//@required
//- (void)fetchTips:(Place*)place withCompletion:(TipsCompletionBlock)withCompletion;
//
//@required
//- (void)fetchImageURL:(Place*)place withCompletion:(ImageURLStringCompletionBlock)withCompletion;

@end
